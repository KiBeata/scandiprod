<?php
namespace App\Models;


use App\Sysclasses\DBHandler;

class Model {

	/**
	** The connection object itself
	**/
	protected static $connection = null;

	/**
	** Constructor
	**/
	public function __construct() {
		self::$connection = $this->conn();
	}

	/**
	** Connect to the database
	**
	**/
	protected function conn() {
		return \App\Sysclasses\DBHandler::conn();
	}
}