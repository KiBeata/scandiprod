<?php

namespace App\Models;

class Product extends Model{

	/**
	** Constructor
	**
	**/
	public function __construct() {
		parent::__construct();
	}


	/**
	** Gets all the products
	**
	** @return array|string
	**/
	public static function getAll() {
		$query  = "select products.id, products.sku, products.name, products.price, CONCAT( product_propset.prop_name, ";
		$query .= "': ', product_values.prop_val) as prop from products join product_values on ";
		$query .= "product_values.product_id=products.id join  product_propset on product_propset.id = ";
		$query .= "product_values.propset_id join product_type on product_type.id = product_propset.type_id ";
		$query .= "where product_type.name!='Furniture' ";
		$query .= "UNION select products.id, products.sku, products.name, products.price, CONCAT('Dimension: ', ";
		$query .= "GROUP_CONCAT(SUBSTR(product_values.prop_val,1,POSITION(' ' in product_values.prop_val)-1) ";
		$query .= "SEPARATOR 'x')) as prop from products join product_values on ";
		$query .= "product_values.product_id=products.id join product_propset on ";
		$query .= "product_propset.id=product_values.propset_id join product_type on ";
		$query .= "product_type.id=product_propset.type_id where product_type.name='Furniture' group by products.id;";
		try {
			$result = self::$connection->query($query);
			if($result) {
				$ret = $result->fetch_all();
				$result->free_result();
 				return $ret;
			} else {
				return [];
			}
		} catch(Error $e) {
			return $e->getMessage();
		}
	}


	/**
	** Deletes all the gitven products
	**
	** @param $ids array
	** @return bool|string
	**/
	public static function delete($ids) {
		$query = "delete from products where products.id in ( " . implode(',', $ids) . " );";
		try {
			$result = self::$connection->query($query);
			if($result) {
				return true;
			} else {
				return false;
			}
		} catch(Error $e) {
			return $e->getMessage();
		}
	}

	/**
	** Gets all the product types
	**
	** @return array|string
	**/
	public static function getTypes() {
		$query = "select id, name from product_type;";
		try {
			$result = self::$connection->query($query);
			if($result) {
				$ret = $result->fetch_all();
				$result->free_result();
 				return $ret;
			} else {
				return [];
			}
		} catch(Error $e) {
			return $e->getMessage();
		}
	}

	/**
	** Adds a product with gitven data
	**
	** @param $data array
	** @return int|bool|string
	**/
	public static function addOne($data) {
		$query = "insert into products (id, sku, name, date_added, date_updated, price) VALUES ";
		$query .= "( NULL, '" . $data['sku'] . "', '" . $data['name'] . "', NULL, NULL, " . $data['price'] . ");";
		try {
			$result = self::$connection->query($query);
			if($result) {
				return self::$connection->query("select last_insert_id() as id;")->fetch_assoc();
			} else {
				return false;
			}
		} catch(Error $e) {
			return $e->getMessage();
		}
	}

	/**
	** Adds a product with gitven data
	**
	** @param $data array
	** @return int|bool|string
	**/
	public static function getPropSets($type_id=null) {
		$query = "select id, prop_name from product_propset ";
		if($type_id != null ) $query .= "where type_id=" . $type_id;
		$query .= ";";
		try {
			$result = self::$connection->query($query);
			if($result) {
				$ret = $result->fetch_all();
				$result->free_result();
 				return $ret;
			} else {
				return [];
			}
		} catch(Error $e) {
			return $e->getMessage();
		}
	}

	/**
	** Adds a products properties
	**
	** @param $rowdata array
	** @return bool|string
	**/
	public static function addPropRow($rowdata) {
		$query = "insert into product_values VALUES ";
		$query .= "( NULL, " . implode(',', $rowdata) . ")";
		try {
			$result = self::$connection->query($query);
			if($result) {
				return true;
			} else {
				return false;
			}
		} catch(Error $e) {
			return $e->getMessage();
		}
	}


	
}