<html lang="en">
	<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, inital-scale=1">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="locale" content="en">
			<meta content="text/html;charset=utf-8;X-Content-Type-Options=nosniff" http-equiv="Content-Type">
			<meta content="utf-8" http-equiv="encoding">
			<link rel="stylesheet" href="/resources/css/app.css"  type="text/css"/>
			<link rel="icon" sizes="16x16" type="image/png" href="/resources/imgs/favicon.ico">
			
	</head>
	<body>
		<div class="page">
			<article class="content">
				<section class="band">
					<h1><?php echo $params['viewParams']['title'];?></h1>
					<div>
						<a href="<?php echo $params['viewParams']['links'][0]; ?>" id="<?php echo $params['viewParams']['ids'][0];?>"><?php echo $params['viewParams']['blabels'][0];?></a>
						<input type="button" name="" value="<?php echo $params['viewParams']['blabels'][1];?>" id="<?php echo $params['viewParams']['ids'][1];?>">
					</div>
				</section>
			<?php require_once(_VIEWPARTPATH . $viewpart . '.php'); ?>
			</article>
		</div>

		<article class="feet">
			<section>Scandiweb Test assignment</section>
		</article>
		<footer>
			<script type="text/javascript" src="/resources/js/app.js"></script>
		</footer>
	</body>
	
</html>