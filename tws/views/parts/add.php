<div id="errspc"></div>
<form id="product_form" method="POST" action="/products/save">
	<div class="frow">
		<label for="sku">SKU</label>
		<input type="text" name="sku" id="sku" minlength="9" placeholder="sku goes here..." /> 
	</div>
	<div class="frow">
		<label for="name">Name</label>
		<input type="text" name="name" id="name" placeholder="name goes here..." /> 
	</div>
	<div class="frow">
		<label for="price">Price ($)</label>
		<input type="text" name="price" id="price" placeholder="price goes here..."/> 
	</div>
	<div class="frow">
		<label for="type">Type</label>
		<select name="type" id="productType"/>
			<option value="-1">
				Type Switcher
			</option>
			<?php foreach ($params['data']['types'] as $key => $value):?>
				<option value="<?php echo $value[0];?>"><?php echo $value[1];?></option>
			<?php endforeach;?>
		</select>
	</div>
	<div class="optblock" id="DVD">
		<div class="frow">
			<label for="size">Size (MB)</label>
			<input type="text" name="size" id="size" placeholder="size goes here ...">
		</div>
		<div class="frow">
			<desc>Please, provide size</desc>
		</div>
	</div>
	<div class="optblock" id="Book">
		<div class="frow">
			<label for="weight">Weight (KG)</label>
			<input type="text" name="weight" id="weight" placeholder="weight goes here ...">
		</div>
		<div class="frow">
			<desc>Please, provide weight</desc>
		</div>
	</div>
	<div class="optblock" id="Furniture">
		<div class="frow">
			<label for="height">Height (CM)</label>
			<input type="text" name="height" id="height" placeholder="height goes here ...">
		</div>
		<div class="frow">
			<label for="width">Width (CM)</label>
			<input type="text" name="width" id="width" placeholder="width goes here ...">
		</div>
		<div class="frow">
			<label for="length">Length (CM)</label>
			<input type="text" name="length" id="length" placeholder="length goes here ...">
		</div>
		<div class="frow">
			<desc>Please, provide dimensions</desc>
		</div>
	</div>
</form>