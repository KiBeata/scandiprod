
	<section class="boxlist">
		<?php foreach($params['data'] as $part):?>
			<div class="pbox">
				<div class="details">
					<div class="actions">
						<input type="checkbox" id="<?php echo 'check-' . $part[0];?>" class="delete-checkbox"/>
					</div>
					<div class="data">
						<div><?php echo $part[1];?></div>
						<div><?php echo $part[2];?></div>
						<div><?php echo $part[3];?> $</div>
						<div><?php echo $part[4];?></div>
					</div>
				</div>
			</div>
		<?php endforeach;?>
	</section>
	