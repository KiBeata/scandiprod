<?php

namespace App\Controllers;


class ResourcesController extends Controller{
	
	/**
	** It loads the styles
	**
	** @param $rname string
	**/
	public function css($rname = 'app.css') {
		header("content-type:text/css;");
		require_once(_RESPATH . "/" . $rname);
	}

	/**
	** It loads the javascripts
	**
	** @param $rname string
	**/
	public function js($rname = 'app.js') {
		header("content-type:text/javascript;");
		require_once(_RESPATH . "/js/" . $rname);
	}

	/**
	** It loads the images
	**
	** @param $rname string
	**/
	public function imgs($rname = 'favicon.ico') {
		require_once(_RESPATH . "/imgs/" . $rname);
	}
}