<?php

namespace App\Controllers;

use App\Models\Product;

class ProductsController extends Controller{
	
	/**
	** It loads the product list page
	**/
	public function index() {
		$this->all();
	}

	/**
	** Lists all the products
	** 
	**/
	public function all() {  
		$pmod = new \App\Models\Product();
		$products = $pmod->getAll();
		uasort($products, function ($a, $b) {
			   if (intval($a[0]) === intval($b[0])) {
			        return 0;
			    }
			    return (intval($a[0]) < intval($b[0])) ? -1 : 1;
			   });
		$viewParams = ['title' => 'Product list',
						'blabels' => ['ADD','MASS DELETE'],
						'links' => ['/products/add-product', ''],
						'ids' => ['','delete-product-btn']];
		$this->getView('all', ['data' => $products, 'viewParams' => $viewParams]);
	}

	/**
	** Deletes selected products
	**
	** @method POST
	** @param $id
	** @return json
	**/

	public function delete($id=null) {
		$val = array_pop($_POST);
		$ids =  explode(',',trim(substr($val, 6, strpos($val, "--")-6)));
		$pmod = new \App\Models\Product();
		echo json_encode(['deleted'=>$pmod::delete($ids)]);
		exit;
	}

	/**
	** Shows the add product view
	**
	** 
	**/
	public function addproduct() {
		$viewParams = ['title' => 'Product add',
						'blabels' => ['Save','Cancel'],
						'links' => ['#', ''],
						'ids' => ['save-data-btn','cancel-data-btn']];
		$pmod = new \App\Models\Product();
		$this->getView('add', ['viewParams' => $viewParams, 'data' => ['types' => $pmod->getTypes()]]);
	}

	/**
	** Saves the product, that is to be inserted
	**
	** @method POST
	**/
	public function save() {
		$pmod = new \App\Models\Product();
		$tblData = ['name'=>$_POST['name'],
					'sku' => $_POST['sku'],
					'price' => $_POST['price']];
		$pid = $pmod->addOne($tblData);
		if($pid) {
			$propSets = $pmod->getPropsets($_POST['type']);
			foreach ($propSets as $key => $value) {
				$valrow = ['propset_id' => $value[0],
							'product_id' => $pid['id'],
							'prop_val' => "'" . $_POST[strtolower($value[1])] . "'"];
				$pmod->addPropRow($valrow);
			}
		}
		header('Location: /products/all');
	}
}