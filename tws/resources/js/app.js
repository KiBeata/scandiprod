var elem = document.getElementById('delete-product-btn');
if(elem) {
	elem.addEventListener('click', event => {
	  let prods = document.querySelectorAll('[id^="check-"]:checked');
	  let ids = [];
	  prods.forEach(item => {
	  	ids.push(item.id.substring(6));
	  });
	  let xhr = new XMLHttpRequest();
	  xhr.open("POST", "/products/delete", true);
	  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	  xhr.responseType = 'json';
	  xhr.onreadystatechange = function (resp) 
	  	{
	        if (xhr.readyState == 4 && xhr.status == 200)
	        {
	            if(xhr.response.deleted === true) {
	            	window.location.reload();
	            }
	        }
	    }
	  var formData = new FormData();
	  formData.append("ids", ids);
	  var data = {'ids': ids } ;
	  xhr.send(formData);
	});
} else {
	elem = document.getElementById('cancel-data-btn');
	elem.addEventListener('click', event => {
		window.location.href= window.location.pathname.substr(0,window.location.pathname.lastIndexOf("/")+1) + 'all';
	});
	var typeBtn = document.getElementById('productType');
	typeBtn.addEventListener('change', event => {
		let errspc = document.getElementById('errspc');
		errspc.style.display = 'none';
		let opts = document.getElementsByClassName('optblock');
		for (var i = opts.length - 1; i >= 0; i--) {
			opts[i].style.display = 'none';
		}
		let idOf = typeBtn.selectedOptions[0].text;
		document.getElementById(idOf).style.display = 'flex';
	});
	function isFilled(inputs, select) {
		if(inputs[0].value.length === 0 || inputs[1].value.length === 0 || inputs[2].value.length === 0) {
			return false;
		}
		if(select.selectedOptions[0].value == -1) {
			return false;
		}
		let optblocks = document.getElementsByClassName('optblock');
		let optelems = optblocks[parseInt(select.selectedOptions[0].value)-1].children;
		for (let i = 0;  i < optelems.length - 1; i++) {
			if (optelems[i].children[1].value.length === 0 ) 
				return false;
		}
		return true;
	}
	function areNumeric(optind=-1) {
		let price = document.getElementById('price');
		if(isNaN(price.value))
			return false;
		let optblocks = document.getElementsByClassName('optblock');
		let optelems = optblocks[parseInt(optind)-1].children;
		for (let i = 0;  i < optelems.length - 1; i++) {
			if (isNaN(optelems[i].children[1].value) ) 
				return false;
		}
		return true;
	}
	function decorate(optind=-1) {
		let optblocks = document.getElementsByClassName('optblock');
		let optelems = optblocks[parseInt(optind)-1].children;
		for (let i = 0;  i < optelems.length - 1; i++) {
			let tmp =optelems[i].children[0].innerHTML.trim();
			tmp = tmp.substring(tmp.indexOf('(')+1, tmp.length-1);
			optelems[i].children[1].value = optelems[i].children[1].value + " " + tmp; 
		}
	}
	var saveBtn = document.getElementById('save-data-btn');
	saveBtn.addEventListener('click', event => {  
		let err = 'Please, submit required data';
		let errspc = document.getElementById('errspc');
		let tmps = document.getElementById('sku').value;
		let inputs = document.forms["product_form"].getElementsByTagName("input");
		let select = document.getElementById('productType');
		let passed = isFilled(inputs,select);
		if(!passed) {
			errspc.innerHTML = err;
			errspc.style.display = 'flex';
		} else {
			passed = areNumeric(select.selectedOptions[0].value);
			if(!passed) {
				errspc.innerHTML = "Please, provide the data of indicated type";
				errspc.style.display = 'flex';
			} else {
				decorate(select.selectedOptions[0].value);
				var form = document.getElementById('product_form');
				form.submit();
			}
		}
	});
}
