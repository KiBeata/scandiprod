<?php
require_once(__DIR__ . '/sysclasses/Router.php');
require_once(__DIR__ . '/controllers/Controller.php');
require_once(__DIR__ . '/controllers/Products.php');
require_once(__DIR__ . '/controllers/Resource.php');
require_once(__DIR__ . '/sysclasses/DBHandler.php');
require_once(__DIR__ . '/models/Model.php');
require_once(__DIR__ . '/models/Product.php');
use App\Sysclasses\Router;


define("_RESPATH", __DIR__ . '/resources', true);
define("_VIEWPATH", __DIR__ . "/views/layout.php", true);
define("_VIEWPARTPATH", __DIR__ . "/views/parts/", true);
$r = new Router();

