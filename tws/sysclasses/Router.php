<?php

namespace App\Sysclasses;

use App\Controllers\ProductsController;

class Router {
	
	/**
	** Url data
	**/
	private $_url = [];

	
	/**
	** Processes the called URL
	**
	**/
	function __construct() { 
		$_urlpath = explode('/',parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH));
		if($_urlpath[1]=="") $_urlpath[1] = 'products';
		if($_urlpath[2]==NULL) $_urlpath[2] = 'all';
		$this->_url = $_urlpath;
		try {
			$cn = "\\App\\Controllers\\" . ucfirst(rtrim("$_urlpath[1]")) . "Controller";
			$ctr = new $cn(); 
			$_urlpath[2] = str_replace('-', '', $_urlpath[2]);
			$mth = rtrim("$_urlpath[2]");
			$ctr->$mth();	
		} catch(Exception $e) {
			echo $e->getMessage();
			exit;
		} 
	}
}